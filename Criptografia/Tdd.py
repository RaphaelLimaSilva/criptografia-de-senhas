import unittest
from Programa import hash_sha256,hash_md5


class TestStringMethods(unittest.TestCase):
    def teste(self):
            self.assertNotEqual(hash_sha256('senha'),hash_md5('senha'))
    def teste(self):
        self.assertNotEqual(hash_sha256('senha '), hash_md5('senha'))
    def teste(self):
        self.assertEqual(hash_sha256('senha'), hash_sha256('senha'))
    def teste(self):
        self.assertEqual(hash_md5('senha'), hash_md5('senha'))


if __name__ == '__main__':
    unittest.main()

